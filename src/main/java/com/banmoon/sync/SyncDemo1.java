package com.banmoon.sync;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

/**
 * sync修饰对象时
 */
public class SyncDemo1{

    private void printOne(){
        synchronized (this){
            try {
                Thread.sleep(2000);
                String format = StrUtil.format("{}：当前this对象：{}，时间：{}", Thread.currentThread().getName(), this, DateUtil.now());
                System.out.println(format);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void printTwo(){
        synchronized (this){
            String format = StrUtil.format("{}：当前this对象：{}，时间：{}", Thread.currentThread().getName(), this, DateUtil.now());
            System.out.println(format);
        }
    }

    public static void main(String[] args) {
        SyncDemo1 syncDemo1 = new SyncDemo1();
        new Thread(() -> {
            syncDemo1.printOne();
        }, "线程A").start();
        new Thread(() -> {
            syncDemo1.printTwo();
        }, "线程B").start();
//        System.out.println(syncDemo1);// 指的就是31行创建出来的对象
    }

}

