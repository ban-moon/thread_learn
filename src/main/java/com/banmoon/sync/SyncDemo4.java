package com.banmoon.sync;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

public class SyncDemo4 {

    private static synchronized void printOne(){
        try {
            Thread.sleep(2000);
            String format = StrUtil.format("{}：时间：{}", Thread.currentThread().getName(), DateUtil.now());
            System.out.println(format);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void printTwo(){
        synchronized (this){
//        synchronized (SyncDemo4.class){// 判断获取的是否是SyncDemo4.class对象
            String format = StrUtil.format("{}：时间：{}", Thread.currentThread().getName(), DateUtil.now());
            System.out.println(format);
        }
    }

    public static void main(String[] args) {
        SyncDemo4 syncDemo4 = new SyncDemo4();
        new Thread(() -> {
            syncDemo4.printOne();
        }, "线程A").start();
        new Thread(() -> {
            syncDemo4.printTwo();
        }, "线程B").start();
    }
}
