package com.banmoon.sync;

public class SyncException {

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            synchronized (SyncException.class){
                for (int i = 0; i < 20; i++) {
                    if(i==10)
                        i = 1/0;// 异常
                    System.out.println("线程A：" + i);
                }
            }
        }).start();

        Thread.sleep(3000);
        synchronized (SyncException.class){
            for (int i = 0; i < 10; i++) {
                System.out.println("主线程");
            }
        }
    }

}
