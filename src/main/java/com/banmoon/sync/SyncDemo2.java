package com.banmoon.sync;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

/**
 * 当修饰class对象时
 */
public class SyncDemo2 {

    private void printOne(){
        synchronized (SyncDemo2.class){
            try {
                Thread.sleep(2000);
                String format = StrUtil.format("{}：时间：{}", Thread.currentThread().getName(), DateUtil.now());
                System.out.println(format);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void printTwo(){
        synchronized (SyncDemo2.class){
            String format = StrUtil.format("{}：时间：{}", Thread.currentThread().getName(), DateUtil.now());
            System.out.println(format);
        }
    }

    public static void main(String[] args) {
        SyncDemo2 syncDemo2 = new SyncDemo2();
        new Thread(() -> {
            syncDemo2.printOne();
        }, "线程A").start();
        new Thread(() -> {
            syncDemo2.printTwo();
        }, "线程B").start();
//        System.out.println(SyncDemo2.class);// class对象具有唯一性
    }
}
