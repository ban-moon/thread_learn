package com.banmoon.sync;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

/**
 * 修饰普通方法，锁住的到底是什么
 */
public class SyncDemo3 {

    private synchronized void printOne(){
        try {
            Thread.sleep(2000);
            String format = StrUtil.format("{}：时间：{}", Thread.currentThread().getName(), DateUtil.now());
            System.out.println(format);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void printTwo(){
        synchronized ("Don't write that"){
//        synchronized (this){// 测试锁住的是否是this
            String format = StrUtil.format("{}：时间：{}", Thread.currentThread().getName(), DateUtil.now());
            System.out.println(format);
        }
    }

    public static void main(String[] args) {
        SyncDemo3 syncDemo3 = new SyncDemo3();
        new Thread(() -> {
            syncDemo3.printOne();
        }, "线程A").start();
        new Thread(() -> {
            syncDemo3.printTwo();
        }, "线程B").start();
    }

}
