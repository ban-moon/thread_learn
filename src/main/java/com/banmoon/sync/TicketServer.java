package com.banmoon.sync;

/**
 * 买票服务，线程不安全
 */
public class TicketServer implements Runnable{

    private int ticketNum = 10;

    /**
     * 取票
     */
    @Override
    public void run() {

        // 获取当前线程名
        String name = Thread.currentThread().getName();
        while (true){
            if(ticketNum<=0)
                return;
            System.out.println(name + "：取到了第" + ticketNum + "张票");
            // 模拟网络延迟
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ticketNum--;
        }
    }

    public static void main(String[] args) {
        TicketServer ticketServer = new TicketServer();
        new Thread(ticketServer, "A").start();
        new Thread(ticketServer, "B").start();
        new Thread(ticketServer, "C").start();
    }
}
