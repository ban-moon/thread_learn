package com.banmoon.collection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

/**
 * 线程不安全的集合
 */
public class Demo1 {

    public static void main(String[] args) {
        testArrayList();
//        testHashMap();
//        testSet();
    }

    public static void testArrayList(){
        ArrayList<String> list = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            new Thread(() -> {
                list.add(Thread.currentThread().getName());
                System.out.println(list);
            }, "线程" + i).start();
        }
    }

    public static void testHashMap(){
        HashMap<Integer, String> map = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            int temp = i;
            new Thread(() -> {
                map.put(temp, Thread.currentThread().getName());
                System.out.println(map);
            }, "线程" + i).start();
        }
    }

    public static void testSet(){
        TreeSet<String> set = new TreeSet<>();
        for (int i = 1; i <= 10; i++) {
            new Thread(() -> {
                set.add(Thread.currentThread().getName());
                System.out.println(set);
            }, "线程" + i).start();
        }
    }

}