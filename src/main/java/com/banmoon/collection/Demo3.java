package com.banmoon.collection;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * juc并发包下的集合
 */
public class Demo3 {

    public static void main(String[] args) {
        testArrayList();
//        testHashMap();
//        testSet();
    }

    public static void testArrayList(){
        CopyOnWriteArrayList<Object> list = new CopyOnWriteArrayList<>();
        for (int i = 1; i <= 10; i++) {
            new Thread(() -> {
                list.add(Thread.currentThread().getName());
                System.out.println(list);
            }, "线程" + i).start();
        }
    }

    public static void testHashMap(){
        ConcurrentHashMap<Integer, String> map = new ConcurrentHashMap<>();
        for (int i = 0; i < 10; i++) {
            int temp = i;
            new Thread(() -> {
                map.put(temp, Thread.currentThread().getName());
                System.out.println(map);
            }, "线程" + i).start();
        }
    }

    public static void testSet(){
        CopyOnWriteArraySet<String> set = new CopyOnWriteArraySet<>();
        for (int i = 1; i <= 10; i++) {
            new Thread(() -> {
                set.add(Thread.currentThread().getName());
                System.out.println(set);
            }, "线程" + i).start();
        }
    }

}
