package com.banmoon.mode;

/**
 * 实现多线程方式
 * 1、实现接口Runnable，Lambda简写方式
 */
public class RunnableModeByLambda {

    public static void main(String[] args) {
        new Thread(() -> {
            for (int i = 0; i < 1000; i++)
                System.out.println("=========== 线程A ===========");
        }).start();
        new Thread(() -> {
            for (int i = 0; i < 1000; i++)
                System.out.println("=========== 线程B ===========");
        }).start();
        for (int i = 0; i < 1000; i++)
            System.out.println("=========== 主线程 ===========");
    }
}
