package com.banmoon.mode;

import java.util.concurrent.*;

/**
 * 实现多线程方式
 * 1、实现Callable接口，了解
 * 2、需要定义返回值类型
 * 3、创建执行服务线程池，来进行执行
 */
public class CallableMode {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(2);
        Future<String> resultA = service.submit(new CallableModeA());
        Future<String> resultB = service.submit(new CallableModeB());
        System.out.println("结果A：" + resultA.get());
        System.out.println("结果B：" + resultB.get());
        for (int i = 0; i < 1000; i++)
            System.out.println("=========== 主线程 ===========");
        service.shutdown();
    }

}

class CallableModeA implements Callable<String>{
    @Override
    public String call() throws Exception {
        String str = "=========== 线程A ===========";
        for (int i = 0; i < 1000; i++)
            System.out.println(str);
        return str;
    }
}

class CallableModeB implements Callable<String>{
    @Override
    public String call() throws Exception {
        String str = "=========== 线程B ===========";
        for (int i = 0; i < 1000; i++)
            System.out.println(str);
        return str;
    }
}