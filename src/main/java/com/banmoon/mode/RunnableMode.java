package com.banmoon.mode;

/**
 * 实现多线程方式
 * 1、实现接口Runnable
 * 2、构造Thread对象，将Runnable实现对象作为参数
 * 3、调用Thread对象的start方法
 */
public class RunnableMode {

    public static void main(String[] args) {
        Thread modeA = new Thread(new RunnableModeA());
        Thread modeB = new Thread(new RunnableModeB());
        modeA.start();
        modeB.start();
        for (int i = 0; i < 1000; i++)
            System.out.println("=========== 主线程 ===========");
    }
}

class RunnableModeA implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("=========== 线程A ===========");
        }
    }
}

class RunnableModeB implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("=========== 线程B ===========");
        }
    }
}