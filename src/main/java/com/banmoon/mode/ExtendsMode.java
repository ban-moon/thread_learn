package com.banmoon.mode;

/**
 * 实现多线程方式
 * 1、继承类Thread
 * 2、实现其run方法
 * 3、创建该对象，调用start方法
 */
public class ExtendsMode{

    public static void main(String[] args) {
        ExtendsModeA modeA = new ExtendsModeA();
        ExtendsModeB modeB = new ExtendsModeB();
        modeA.start();
        modeB.start();
        for (int i = 0; i < 1000; i++)
            System.out.println("=========== 主线程 ===========");
    }
}

class ExtendsModeA extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("=========== 线程A ===========");
        }
    }
}

class ExtendsModeB extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("=========== 线程B ===========");
        }
    }
}