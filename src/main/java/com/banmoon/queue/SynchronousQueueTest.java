package com.banmoon.queue;

import cn.hutool.core.util.StrUtil;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

public class SynchronousQueueTest {

    public static void main(String[] args) {
        SynchronousQueue<String> queue = new SynchronousQueue<>();
        new Thread(() -> {
            try {
                String name = Thread.currentThread().getName();
                queue.put("A");
                System.out.println(name + "插入A成功");
                TimeUnit.SECONDS.sleep(1);
                queue.put("B");
                System.out.println(name + "插入B成功");
                TimeUnit.SECONDS.sleep(1);
                queue.put("C");
                System.out.println(name + "插入C成功");
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "线程A").start();

        new Thread(() -> {
            try {
                String name = Thread.currentThread().getName();
                String template = "{}取出了{}";
                System.out.println(StrUtil.format(template, name, queue.take()));
                TimeUnit.SECONDS.sleep(1);
                System.out.println(StrUtil.format(template, name, queue.take()));
                TimeUnit.SECONDS.sleep(1);
                System.out.println(StrUtil.format(template, name, queue.take()));
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "线程B").start();

    }

}
