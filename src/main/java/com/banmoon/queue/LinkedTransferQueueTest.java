package com.banmoon.queue;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

import java.util.Date;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TimeUnit;

public class LinkedTransferQueueTest {

    public static void main(String[] args) throws InterruptedException {
//        test1();
//        test2();
        test3();
    }

    /**
     * 效果与SynchronousQueue基本一致
     * transfer插入时，没有匹配的取操作则会阻塞
     * @throws InterruptedException
     */
    private static void test1() throws InterruptedException {
        LinkedTransferQueue<String> queue = new LinkedTransferQueue<>();

        new Thread(() -> {
            try {
                for (int i = 0; i < 3; i++) {
                    String str = queue.take();
                    TimeUnit.SECONDS.sleep(1);
                    System.out.println(StrUtil.format("取到了{}，{}", str, DateUtil.formatTime(new Date())));
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        queue.transfer("A");
        System.out.println("插入了A");
        queue.transfer("B");
        System.out.println("插入了B");
        queue.transfer("C");
        System.out.println("插入了C");
    }

    /**
     * 存元素时
     * 如果有取操作阻塞的话，则进行匹配，返回true
     * 没有取操作的话，不阻塞，直接返回false
     */
    private static void test2() throws InterruptedException {
        LinkedTransferQueue<String> queue = new LinkedTransferQueue<>();

        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                try {
                    System.out.println(StrUtil.format("取到了{}，{}", queue.take(), DateUtil.formatTime(new Date())));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }

        // 等待三个取的操作都执行完
        TimeUnit.SECONDS.sleep(2);

        System.out.println(StrUtil.format("插入A：{}，{}", queue.tryTransfer("A"), DateUtil.formatTime(new Date())));
        System.out.println(StrUtil.format("插入B：{}，{}", queue.tryTransfer("B"), DateUtil.formatTime(new Date())));
        System.out.println(StrUtil.format("插入C：{}，{}", queue.tryTransfer("C"), DateUtil.formatTime(new Date())));
    }

    /**
     * 存操作时，没有取操作匹配，将会等待一段时间再进行返回
     * @throws InterruptedException
     */
    private static void test3() throws InterruptedException {
        LinkedTransferQueue<String> queue = new LinkedTransferQueue<>();

        new Thread(() -> {
            for (int i = 0; i < 3; i++) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                    System.out.println(StrUtil.format("取到了{}，{}", queue.take(), DateUtil.formatTime(new Date())));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        System.out.println(StrUtil.format("插入A：{}，{}", queue.tryTransfer("A", 3, TimeUnit.SECONDS), DateUtil.formatTime(new Date())));
        System.out.println(StrUtil.format("插入B：{}，{}", queue.tryTransfer("B", 3, TimeUnit.SECONDS), DateUtil.formatTime(new Date())));
        System.out.println(StrUtil.format("插入C：{}，{}", queue.tryTransfer("C", 3, TimeUnit.SECONDS), DateUtil.formatTime(new Date())));
    }


}
