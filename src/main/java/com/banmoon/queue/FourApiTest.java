package com.banmoon.queue;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class FourApiTest {

    public static void main(String[] args) throws InterruptedException {
//        test1();
//        test2();
//        test3();
        test4();
    }

    /**
     * 会抛出异常
     */
    private static void test1() {
        ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<>(2);
        System.out.println(queue.add("A"));
        System.out.println(queue.add("B"));
//        System.out.println(queue.add("C"));// 队列已满，将报错

        System.out.println("======== 分割线 ========");

        System.out.println(queue.remove());
        System.out.println(queue.remove());
//        System.out.println(queue.remove());// 队列已空，将报错
    }

    /**
     * 满了还去存则直接返回false，空了还去取就会返回null
     */
    private static void test2() {
        ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<>(2);
        System.out.println(queue.offer("A"));
        System.out.println(queue.offer("B"));
        System.out.println(queue.offer("C"));// 队列已满，存失败就直接返回false

        System.out.println("======== 分割线 ========");

        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.poll());// 队列已空，直接返回null
    }

    /**
     * 满了还去存就会一直阻塞，直到被唤醒，空了还去取就会一直阻塞，直到被唤醒
     * @throws InterruptedException
     */
    private static void test3() throws InterruptedException {
        ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<>(2);
        queue.put("A");
        System.out.println("A已插入");
        queue.put("B");
        System.out.println("B已插入");
//        queue.put("C");// 队列已满，到此将会阻塞
//        System.out.println("C已插入");

        System.out.println("======== 分割线 ========");

        System.out.println(queue.take());
        System.out.println(queue.take());
//        System.out.println(queue.take());// 队列已空，将会持续阻塞
    }

    /**
     * 满了还去存就会阻塞一段时间，超过后就返回false
     * 空了还去取就会阻塞一段时间，超过时间后就会返回null
     */
    private static void test4() throws InterruptedException {
        ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<>(2);
        String template = "{}：{}";
        System.out.println(StrUtil.format(template, queue.offer("A", 2, TimeUnit.SECONDS), DateUtil.now()));
        System.out.println(StrUtil.format(template, queue.offer("B", 2, TimeUnit.SECONDS), DateUtil.now()));
        System.out.println(StrUtil.format(template, queue.offer("C", 2, TimeUnit.SECONDS), DateUtil.now()));// 队列已满，等待2秒后返回false

        System.out.println("======== 分割线 ========");

        System.out.println(StrUtil.format(template, queue.poll(2, TimeUnit.SECONDS), DateUtil.now()));
        System.out.println(StrUtil.format(template, queue.poll(2, TimeUnit.SECONDS), DateUtil.now()));
        System.out.println(StrUtil.format(template, queue.poll(2, TimeUnit.SECONDS), DateUtil.now()));// 队列已空，等待2秒后返回null
    }

}
