package com.banmoon.queue;

import java.util.concurrent.LinkedBlockingDeque;

public class LinkedBlockingDequeTest {

    public static void main(String[] args) throws InterruptedException {
        LinkedBlockingDeque<String> deque = new LinkedBlockingDeque<>(4);
        deque.put("A");
        deque.put("B");
        deque.put("C");
        deque.putFirst("D");

        System.out.println(deque.take());
        System.out.println(deque.takeLast());
    }

}
