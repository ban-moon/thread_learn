package com.banmoon.queue;

import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;

public class PriorityBlockingQueueTest {

    public static void main(String[] args) throws InterruptedException {
        PriorityBlockingQueue<Integer> queue = new PriorityBlockingQueue<Integer>(3, new MyComparator());
        queue.put(1);
        queue.put(3);
        queue.put(2);

        System.out.println(queue.take());
        System.out.println(queue.take());
        System.out.println(queue.take());

    }

}

class MyComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        if(o1==o2)
            return 0;
        return o1>o2? -1: 1;
    }
}