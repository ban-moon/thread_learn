package com.banmoon.status;

public class ThreadPriorityMethods {

    public static void main(String[] args) {
        Thread threadA = new Thread(new MyThread(), "线程A");
        Thread threadB = new Thread(new MyThread(), "线程B");
        Thread threadC = new Thread(new MyThread(), "线程C");
        threadA.setPriority(9);
        threadB.setPriority(5);
        threadC.setPriority(1);
        threadC.start();
        threadB.start();
        threadA.start();
    }

}

class MyThread implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("============" + Thread.currentThread().getName() + "============");
        }
    }
}