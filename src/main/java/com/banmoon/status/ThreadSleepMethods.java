package com.banmoon.status;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ThreadSleepMethods {

    public static void main(String[] args) {
        new Thread(() -> {
            int i = 0;
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            while (i++<10){
                String dateStr = sdf.format(new Date());
                System.out.println(dateStr + "============ 线程A ============");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}