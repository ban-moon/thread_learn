package com.banmoon.status;

public class ThreadJoinMethods {

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() -> {
            for (int i = 0; i < 200; i++) {
                System.out.println("============= 线程A"+ i +" =============");
            }
        });
        thread.start();
        for (int i = 0; i < 1000; i++) {
            if(i==100)
                thread.join();
            System.out.println("============= 主线程"+ i +" =============");
        }
    }

}
