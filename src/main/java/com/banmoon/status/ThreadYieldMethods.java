package com.banmoon.status;

public class ThreadYieldMethods {

    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                if(i==200)
                    Thread.yield();
                System.out.println("============= 线程A"+ i +" =============");
            }
        });
        thread.start();
        for (int i = 0; i < 1000; i++) {
            if(i==500)
                Thread.yield();
            System.out.println("============= 主线程"+ i +" =============");
        }
    }
}
