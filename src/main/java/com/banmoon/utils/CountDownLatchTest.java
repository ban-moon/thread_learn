package com.banmoon.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class CountDownLatchTest {

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch count = new CountDownLatch(5);
        for (int i = 1; i <= 5; i++) {
            new Thread(() -> {
                try {
                    System.out.println(StrUtil.format("{}：{}", Thread.currentThread().getName(), DateUtil.now()));
                    TimeUnit.SECONDS.sleep(2);
                    count.countDown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }, "线程"+i).start();
        }

        // 等待上面几个线程完成，才能进行下面的逻辑
        count.await();
        System.out.println("主线程：" + DateUtil.now());
    }

}

