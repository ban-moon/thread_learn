package com.banmoon.wait;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

public class Demo1 {

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            synchronized (Demo1.class){
                for (int i = 1; i <= 10; i++) {
                    try {
                        System.out.println(StrUtil.format("线程A：{}，时间：{}", i, DateUtil.now()));
                        if(i==5){
                            System.out.println("睡一会，释放锁");
                            Demo1.class.wait();
                        }
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        // 等待3秒
        Thread.sleep(5000);

        System.out.println("主线程：唤醒");
        Demo1.class.notify();
        synchronized (Demo1.class){
            System.out.println("主线程：唤醒");
            Demo1.class.notify();
        }

    }

}
